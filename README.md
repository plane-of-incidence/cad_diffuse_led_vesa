# cad_diffuse_led_vesa

## diffuse_led_vesa_75_ws10

### SVG
![](./diffuse_led_vesa_75_ws10/diffuse_led_vesa_75_ws10.svg)

### [STL](./diffuse_led_vesa_75_ws10/diffuse_led_vesa_75_ws10-Body.stl)

### [Freecad Model](./diffuse_led_vesa_75_ws10/diffuse_led_vesa_75_ws10.FCStd)

